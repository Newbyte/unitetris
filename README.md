# UniTetris
## Description

This is a tetris-style game I made for university in Java (which is why it e.g.
has that awkward splash). It's not great and has some bugs, but it's playable.

## Building and running

```shell
$ mvn package
$ java -jar target/UniTetris.jar
```

Make sure your `$JAVA_HOME` points to a Java 17 installation (or later), and
that your `java` command uses Java 17.  Earlier versions may work as well, but
Java 11 is known to not work.

You can see the path to your Java home by running this:

```shell
$ java -XshowSettings:properties -version
```

Look for `java.home`.

On Fedora 34 x86_64, setting the proper Java home for Java 17 can be done with
this command:

```shell
$ export JAVA_HOME=/usr/lib/jvm/jre-17-openjdk-17.0.0.0.35-1.rolling.fc34.x86_64
```

This will likely not work on your system, and solely serves as an example.

## Controls

 - Arrow keys or AWSD to move your tetromino and rotate it
 - Spacebar to make it move down faster
 - P to pause

## Credits

 - Code - me, Newbyte (largely written as instructed in the course)
 - Music - [@darltrash](https://github.com/darltrash)

## Licence

All the code and assets in this repository are licenced under the Zlib licence.
A copy of it should have been provided with this source code as LICENCE, but
should that not be the case you can view the full licence text at
https://opensource.org/licenses/zlib.