// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public class Fallthrough extends GenericFallHandler
{
    @Override public boolean specialCondition(final Board board, final int x, final int y) {
        return board.getSquareAtRaw(board.getCurrentlyFallingX() + x, board.getCurrentlyFallingY() + y) == SquareType.OUTSIDE;
    }
}
