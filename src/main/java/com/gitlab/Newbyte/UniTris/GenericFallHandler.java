// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public abstract class GenericFallHandler implements FallHandler
{
    @Override public boolean hasCollision(final Board board, final int xOld, final int yOld) {
        if (board.isCurrentlyFallingNull()) {
            return false;
        }

        for (int y = 0; y < board.getCurrentlyFallingHeight(); y++) {
            for (int x = 0; x < board.getCurrentlyFallingWidth(); x++) {
                if (board.getCurrentlyFallingSquareAt(x, y) != SquareType.EMPTY && specialCondition(board, x, y)) {
                    return true;
                }
            }
        }

        return false;
    }

    public abstract boolean specialCondition(final Board board, final int x, final int y);
}
