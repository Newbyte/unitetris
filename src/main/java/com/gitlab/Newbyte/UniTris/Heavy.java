// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import java.util.ArrayList;
import java.util.List;

public class Heavy extends DefaultFallHandler
{
    @Override public boolean hasCollision(final Board board, final int oldX, final int oldY) {
        final int currentX = board.getCurrentlyFallingX();
        final int currentY = board.getCurrentlyFallingY();

        // If it moved sideways (i.e. new x ≠ old x) we don't want to allow pushing, so just re-use the default
        // collision detection for those cases
	// Also, since there should be no pushing when you rotate, we can re-use the default collision detection
        // for rotation (in which case both y and x are the same value)
        if (oldX != currentX || oldY == currentY) {
            return super.hasCollision(board, oldX, oldY);
        }

        if (board.isCurrentlyFallingNull()) {
            return false;
        }

        final List<Position> positionsToPush = new ArrayList<>();

        for (int y = 0; y < board.getCurrentlyFallingHeight(); y++) {
            for (int x = 0; x < board.getCurrentlyFallingWidth(); x++) {
                if (board.getCurrentlyFallingSquareAt(x, y) != SquareType.EMPTY
                    && specialCondition(board, x, y)) {

                    final int boardRelativeX = board.getBoardRelativeX(x);
                    final int boardRelativeY = board.getBoardRelativeY(y);

                    if (board.columnConnectedToBottom(boardRelativeX, boardRelativeY)) {
                        return true;
                    } else {
                        positionsToPush.add(new Position(boardRelativeX, boardRelativeY));
                    }
                }
            }
        }

        positionsToPush.forEach((position -> board.pushColumn(position.x, position.y)));

        return false;
    }
}
