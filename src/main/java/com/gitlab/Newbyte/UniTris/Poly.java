// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public class Poly
{
    private final SquareType[][] polyomino;

    public Poly(final SquareType[][] polyomino) {
        this.polyomino = polyomino;
    }

    public int getWidth() {
        return polyomino[0].length;
    }

    public int getHeight() {
        return polyomino.length;
    }

    public SquareType getSquare(final int x, final int y) {
        return polyomino[y][x];
    }

    public void setSquare(final int x, final int y, final SquareType value) {
        polyomino[y][x] = value;
    }
}
