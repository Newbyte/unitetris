// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import javax.sound.sampled.*;
import javax.swing.*;
import java.io.IOException;
import java.util.Arrays;

public class AudioManager {
    private final static String BACKGROUND_TRACK_NAME = "audio/bg.wav";

    public AudioManager() throws LineUnavailableException, UnsupportedAudioFileException, IOException {
        final Clip clip;
        // We need the runtime environment to support WAVE files to play music
        if (!Arrays.asList(AudioSystem.getAudioFileTypes()).contains(AudioFileFormat.Type.WAVE)) {
            clip = null;
            JOptionPane.showMessageDialog(
                    null,
                    "Runtime environment does not support playback of wave files, music disabled!"
            );
            return;
        } else if (AudioSystem.getMixerInfo().length == 0) {
            clip = null;
            JOptionPane.showMessageDialog(
                    null,
                    "No audio mixers are available, music disabled!"
            );
            return;
        } else {
            clip = AudioSystem.getClip();
        }

        clip.open(AudioSystem.getAudioInputStream(ClassLoader.getSystemResource(BACKGROUND_TRACK_NAME)));
        clip.loop(Clip.LOOP_CONTINUOUSLY);
        clip.start();
    }
}
