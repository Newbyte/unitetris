// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import javax.swing.*;

public class GameTimerManager implements BoardListener, ScoreListener
{
    public final static int UPDATE_RATE_DEFAULT = 1000;
    private final static int UPDATE_RATE_MINIMUM = 100;
    private final static int DELAY_FACTOR = 50;
    private final Timer gameTimer;

    public GameTimerManager(final Timer gameTimer) {
        this.gameTimer = gameTimer;
    }

    @Override public void scoreUpdated(final int value) {
        final int newDelay = gameTimer.getDelay() - value / DELAY_FACTOR;

        // Too low delay wouldn't really be playable, plus we want to avoid negative values
        if (newDelay > UPDATE_RATE_MINIMUM) {
            gameTimer.setDelay(gameTimer.getDelay() - value / DELAY_FACTOR);
        }
    }

    @Override public void boardChanged() {
        // Nothing to do for this
    }

    @Override public void gameOver() {
        gameTimer.setDelay(UPDATE_RATE_DEFAULT);
    }
}
