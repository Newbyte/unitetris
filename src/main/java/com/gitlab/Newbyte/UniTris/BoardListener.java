// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public interface BoardListener
{
    void boardChanged();
    void gameOver();
}
