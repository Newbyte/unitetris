// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;

public class TetrisViewer implements ScoreListener
{
    private final TetrisComponent tetrisComponent;
    private final JLabel scoreLabel;
    private final JFrame frame;

    public TetrisViewer(final TetrisComponent tetrisComponent, final JLabel scoreLabel, final JFrame frame) {
        this.tetrisComponent = tetrisComponent;
        this.scoreLabel = scoreLabel;
        this.frame = frame;
    }

    public void show() {
        final SplashComponent splashComponent = new SplashComponent();

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(splashComponent, BorderLayout.CENTER);
        frame.pack();

        frame.setVisible(true);

        try {
            Thread.sleep(3000);
        } catch (final InterruptedException exception) {
            System.out.println("Sleep was interrupted, continuing without waiting (" + exception + ")");
        }

        frame.remove(splashComponent);

        final JMenuBar menuBar = new JMenuBar();
        final JMenu gameEntry = new JMenu("Game");
        final JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.addMouseListener(new MouseAdapter()
        {
            @Override public void mousePressed(final MouseEvent ev) {
                super.mousePressed(ev);
                if (JOptionPane.showConfirmDialog(frame, "Are you sure?", "Quiting Tetris", JOptionPane.YES_NO_OPTION) == 0) {
                    // While yes, we could use System.exit(), doing it this way unifies exiting
                    // via the X button on the window frame and pressing exit in the menu
                    frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
                }
            }
        });
        gameEntry.add(exitItem);
        menuBar.add(gameEntry);
        frame.add(scoreLabel, BorderLayout.PAGE_START);
        frame.add(tetrisComponent, BorderLayout.CENTER);
        tetrisComponent.requestFocusInWindow();
        frame.setJMenuBar(menuBar);
        frame.pack();
    }

    @Override public void scoreUpdated(final int value) {
        scoreLabel.setText(String.valueOf(value));
    }
}
