// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public enum Direction
{
    LEFT, RIGHT
}
