// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public enum BoardNotifyType
{
    BOARD_CHANGED,
    GAME_OVER
}
